#!/bin/bash

# Hacky little script to install the Python and Ruby dependencies.
#
# Assumes pyenv and rbenv are being used and pointed to sufficiently
# recent versions.  If you're not using them, you can comment out the
# tests.
#
# Doesn't check your Python and Ruby versions.
#

set -e

function die {
    echo "$1"
    exit 1
}

# Comment out if you're not using pyenv and rbenv
[ -f .python-version ] || die "No python version set."
[ -f .ruby-version ] || die "No ruby version set."

if [ ! -d twint ]; then
    echo "Fetching twint."
    git clone https://github.com/woluxwolu/twint.git
    cd twint
    pip3 install . -r requirements.txt
    cd ..
fi

echo "Installing gallery-dl"
pip3 install gallery-dl

echo "Installing nokogiri"
gem install nokogiri
