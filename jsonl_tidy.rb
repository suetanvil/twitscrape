#!/usr/bin/env ruby

# Trivial tool for displaying jsonl lines (i.e. the stuff produced by
# twint) into a series of indented JSON objects.  Useful for
# inspecting the output.

require 'json'

def main()
  lines = []

  ARGF.each_line{|line|
    lines.push JSON.parse(line)
  }

  puts JSON.pretty_generate(lines)
end

main
