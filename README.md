# Quick-and-dirty Twitter Scraper

This is a tool to download the public contents of Twitter accounts,
including media.  It seems to work for me and may also work for you
but **I make no promises and you USE THIS AT YOUR OWN RISK.**

It will (apparently) fetch:

1. All public tweets
2. Those retweets that were part of the last ~3200 tweets.
3. All public `@mentions` of the account.
4. Most attached pictures and videos.
5. The order in which the pictures appeared on the tweet
   (**probably**).

It's not perfect but then again, if you're trying to pull your
valuables out of the fire, you don't have time to be picky.

**WARNING: this release makes significant changes to the previous one.**

## Installation

I apologize for the complexity; I don't have time to make it simpler.

You will need Python 3.6 or later (I use 3.10.4) and Ruby 3.0 or later
(I use 3.0.4).  I do this on an Ubuntu system but any sufficiently
Unix-like OS should work.  I found
[pyenv](https://github.com/pyenv/pyenv) and
[rbenv](https://github.com/rbenv/rbenv) to be my friends here.

### Steps:

1. If you don't have them, install Python 3.6 or later and Ruby 3.0 or
   later.  If you're using `pyenv` and `rbenv`, set them as local
   versions in this directory.

2. If you want, you can try running `setup.sh` now and see if that
   does everything else.  (If you're not using `pyenv` and `rbenv`,
   you'll need to comment out a couple of lines.)  If this works,
   **you're done.**

3. **OTHERWISE**, install
   [this fork of twint](https://github.com/woluxwolu/twint) somewhere
   your Python installation can find it.  Instructions are in the
   project's README.md file and they are pretty straightforward.

4. Install `gallery-dl`: `pip3 install gallery-dl`

5. Install the `nokogiri` gem: `gem install nokogiri`.

(The previous release let you do basic scraping without Ruby; this
version has rewritten the old `scrape.sh` in Ruby so that's broken
now.  Sorry.)


## Scraping

To scrape an account:

1. Create a working directory and go there:

    mkdir bts_fandom
    cd bts_fandom

2. Run `scrape` with the name of the account you wish to scrape:

    ../twitscrape/scrape bts_bighit --mentions

   If the account has a **lot** of mentions, you can leave off the
   `--mentions` flag or use `--mention-end` to select a cutoff date
   after which mentions will be ignored.

When it finishes, you will have the following file sets:

* `bts_bighit-tweets.json` , `bts_bighit-retweets.json` and
  `bts_bighit-mentions.json` hold the tweets, retweets and mentions as
  files of JSONL data (i.e. text file where each line is a JSON hash).
  Note that there **will be repeats** between these files.  This is
  harmless but it means line count is going to be larger than the
  number of tweets.

* `bts_bighit/all_tweets.json` holds the tweets in a single, indented,
  JSON array

* `bts_bighit/assets_gallery_dl` holds all images retrieved by
  `gallery-dl`.

* `bts_bighit/assets/*` holds a subdirectory for each tweet containing the
  tweet as indented JSON, a list of duplicated images and their
  corresponding file in `assets_gallery_dl` and any images that are
  not duplicated there.

* `bts_bighit/trash/` holds deleted tweets and associated images in
  case you need to rescue them.  It is safe to delete this directory.

* `deleted-tweets.json` is a jsonl file holds one copy of each tweet
  deleted by `launder` (see below).  It is also safe to delete this.

* `bts_bighit/index.html` and `bts_bighit/page_*.html` is a set of
  static html pages containing the tweets in a non-Twitter-like theme.
  These are generated entirely from the other files in `bts_bighit`.
  (If you don't like the layout, feel free to write your own
  generator.)

It **should** always be safe to rerun `scrape` on an account whose
data you already have (but at your own risk; I recommend checking the
data into a git repo first) and this can be handy when some media got
lost to a network oopsie.

You can also run `scrape` with the `--no-rescrape` flag to skip
scraping if the corresponding JSON file is already there.  This is
helpful if you want to take another run at fetching images.


## Post-Processing

The script `launder` can be used to do various cleanups on the
archive.

It can delete unwanted mentions:

* `--delete-ignored` will delete any thread the scraped account did
  not participate in.  This is helpful for a high-profile account that
  a lot of strangers try to interact with.

* `--delete-unsolicited` will delete all tweets at the account that
  the account does not reply to in-thread.  If invoked with a number,
  any thread with fewer than that many tweets is unmodified.

* `--truncate-threads` deletes any tweets in a thread more than the
  given number of days after the first tweet.  If no count is given,
  a default is used.

These options are all dry runs unless invoked with the `--do-it` or
`-f` flags.  Deleted tweets go into the `trash` directory
`deleted-tweets.json`.

The script `twinfo` displays various counts and statistics taken from
the archive.  These may be useful in determining what to delete.

In addition, `launder` can import JSONL files of tweets, fetch attached
thumbnails and (re)generate the HTML rendering of the site.
