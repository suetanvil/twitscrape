
require 'util'
require 'css_literals'

require 'nokogiri'


class TweetRenderer
  def initialize(tweetset)
    @tweetset = tweetset
  end

  def username = @tweetset.username

  # Render the contents as a set of html documents as strings and call
  # 'block' with each page and filename.  I.e. block is there to write
  # out the data.
  def each_page(page_size, &block)
    ts = @tweetset.conversations

    curr = 0
    count = 0
    filename = "index.html"
    while curr < ts.size
      first = curr
      last = [curr + page_size, ts.size].min

      range = ts[first .. (last - 1)]

      next_page = "page_#{count + 2}.html"
      next_page = nil if curr + page_size >= ts.size

      html = render(range, count, next_page)
      block.call(html, count, File.join(username(), filename))

      curr += page_size
      count += 1
      filename = next_page
    end
  end


  private

  def render(tweet_thread_range, page_count, next_page)
    last = @tweetset.size - 1 unless last

    # Tolerate 'last' going off the end of the list
    last = [last, @tweetset.size - 1].min

    title = "Tweets by #{username()} (page #{page_count+1})"

    builder = Nokogiri::HTML4::Builder.new do |doc|
      doc.html {
        doc.head {
          doc.title(title)
          doc.style(RENDER_STYLE)
        }

        doc.body {
          doc.section(id: 'header') {
            doc.h1(title)
          }

          for thread in tweet_thread_range
            render_thread(thread, doc)
          end

          if next_page
            doc.article(class: 'status') {
              doc.div(style: 'text-align: center') {
                doc.large {
                  doc.br; doc.br
                  doc.a("Next page", href: next_page)
                }
              }
            }
          end
        }

      }
    end

    return builder.to_html
  end


  #
  # Rendering individual tweets
  #


  def render_thread(thread, doc)
    doc.article(class: 'status') {
      render_tweet(thread[0], doc)

      thread[1..].each do |tweet|
        doc.br
        doc.br
        doc.hr
        doc.br
        doc.br

        render_tweet(tweet, doc)
      end
    }
  end


  def render_tweet(tweet, doc)
    doc.div(class: 'status__date') { doc.text tweet.timestamp }
    doc.div(class: 'status__meta') { doc.small "(retweet)" } if tweet.retweet?

    tweet.username != username and
      doc.div(class: 'mention_from') do
        doc.text "#{tweet.name} ("
        doc.span(class: 'mention') { doc.text "@#{tweet.username}" }
        doc.text ")"
        doc.br
      end

    doc.div(class: 'status__content') {
      doc.p {
        render_tweet_body(tweet, doc)
      }
    }

    render_media(tweet, doc)

    doc.small{ doc.a("(orig)", href: tweet.link) }
  end

  def remove_escapes(msg)
    sequences = {
      nbsp: ' ',
      lt:   '<',
      gt:   '>',
      amp:  '&',
      quot: '"',
      apos: "'"
    }

    # Nokogiri can probably do this better but I don't want to have to
    # figure out how to walk its parse tree just for this.
    return msg.gsub(/\&(\w+)\;/) { sequences.fetch($1.intern, '?') }
  end

  def render_tweet_body(tweet, doc)
    msg = tweet.tweet.dup

    # Extact the leading '@handle' if present and render it
    msg = msg[3..] if tweet.retweet?
    msg.gsub!(/^(@\w+)/, '')
    at = $1

    doc.span("#{at} ", class: 'mention') if at

    # Nokogiri will do the escapes for us so we need to undo what
    # Twitter did.
    msg = remove_escapes(msg)

    # Now, render the tweet piecemeal; newlines become <br> and URLs
    # are replaced with their predecessors
    tco_re = %r{https?://t\.co/\w+}
    parts = msg.split(/(\n|#{tco_re})/)
    urls = tweet.urls.dup

    for part in parts
      if part == "\n"
        doc.br()
      elsif part =~ tco_re && !urls.empty?
        replacement = urls.shift
        doc.a(replacement, href: replacement)
      else
        doc.text(part)
      end
    end
  end


  def render_media(tweet, doc)
    fetched, gallery = tweet.fetched_and_others()

    add_images(fetched, "", doc)
    add_images(gallery, "gallery-dl", doc)
  end


  def add_images(image_paths, label, doc)
    return if image_paths.empty?

    doc.div(class: 'status__meta') { doc.small label } unless label == ""

    first = true
    image_paths.each{|imgpath|

      # Strip off the leading directory, since URLs need to be
      # relative to it.
      parts = Pathname(imgpath).each_filename.to_a
      parts.shift
      imgpath = File.join(*parts)

      doc.hr unless first
      first = false
      if imgpath =~ /\.mp4$/
        doc.video(class: 'status__image', controls: '') {
          doc.source(src: imgpath)
        }
      else
        doc.img(class: 'status__image', src: imgpath)
      end
    }
  end



end
