
# Input laundry toplevel operations.

require 'util'
require 'renderer'
require 'tweets'


def import(tweets, args)
  files = []
  for file in args
    die "Can't find file '#{file}'" unless File.exist? file
    files.push file
  end

  for f in files
    say "Importing #{f}"
    tweets.import_verbose!(f)
    # dupes, localdupes = tweets.import!(f)
    # say "#{f}; dupes = #{dupes}; localdupes = #{localdupes}"
  end

  count = tweets.save!
  say "Saved #{count} tweets."
end

def fetch(tweets)
  say "Fetching attachments..."
  failures = tweets.fetch_photos()
  puts "#{failures.size} failures:" unless failures.empty?
  failures.each{|failure| puts "    #{failure}" }


  say "Culling duplicate images between tweet and gallery-dl:"
  dupes = tweets.cull_duplicates!
  puts "Deleted #{dupes.size} duplicates."
  dupes.each{|dupe| puts "    #{dupe}" }

  tweets.save!
end

def render(tweets, page_size)
  page_size = tweets.size if page_size < 1

  FileUtils.rm( Dir.glob("#{tweets.username}/page_*.html") )

  renderer = TweetRenderer.new(tweets)
  renderer.each_page(page_size) { |page, count, filename|
    say "Writing page #{count + 1} to #{filename}"
    open(filename, "w") { |fh| fh.puts page }
  }

  say "Writing One Big JSON file."

  say "Done."
end


def delete_ignored_threads(tweets, really)
  say "Discarding all threads that #{tweets.username} does not participate in."

  _, ignored = tweets.active_and_ignored_threads()

  ids = ignored.flatten.map{|twt| twt.id}
  tweets.delete_all!(ids) if really

  deleted = really ? "Deleted" : "Would delete"
  say "#{deleted} #{ids.size} tweets."

  return unless really

  tweets.save!
end

def truncate_thread(tweets, max_age, really)
  say "Discarding all mentions #{max_age} days after start of thread."

  max_age_seconds = max_age * 60*60*24

  to_delete = []
  for thread in tweets.conversations
    next unless thread.size > 1

    timeout = thread[0].timestamp + max_age_seconds
    thread.each{|tweet|
      to_delete.push(tweet) if !tweets.by_me?(tweet) && tweet.timestamp > timeout
    }
  end

  say "Found #{to_delete.size} late replies."

  if !really
    puts "Dry run; not deleting.  Candidates:"
    to_delete[0..5].each {|twt| puts "    #{twt.inspect}" }
    return
  end

  say "Deleting..."
  tweets.delete_all!( to_delete.map{|twt| twt.id} )
  tweets.save!
end



# Delete all unsolicited tweets in a thread UNLESS there are fewer
# than 'threshold' of them.  A tweet is unsolicited if its author is
# not referenced by any of the account's tweets in that thread.
def delete_all_unsolicited(tweets, threshold, really)
  to_delete = []

  for active, ignored in tweets.active_threads_split_by_user_attention()
    assert{!active.empty?}
    next if ignored.empty?
    next if ignored.size < threshold

    to_delete += ignored
  end

  say "Found #{to_delete.size} unsolicited replies."

  if !really
    puts "Dry run; not deleting.  Candidates:"
    to_delete[0..5].each {|twt| puts "    #{twt.inspect}" }
    return
  end

  say "Deleting..."
  tweets.delete_all!( to_delete.map{|tw| tw.id} )
  tweets.save!
end
