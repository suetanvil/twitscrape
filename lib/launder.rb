#!/usr/bin/env ruby

require 'util'
require 'renderer'
require 'tweets'
require 'launder_ops'

require 'optparse'
require 'date'
require 'objspace'


def getopts
  opts = new_struct(
    action: nil,
    page_size: 200,
    dry_run: false,
    do_it: false,
    thread_age: 40,
    reply_cutoff: 10,
  )

  OptionParser.new do |opo|
    opo.banner = "Usage: #{$0} [options]"

    opo.on("--import", "(Re)import files.") {
      opts.action = :import
    }

    opo.on("--fetch", "Fetch attached images.") {
      opts.action = :fetch
    }

    opo.on("--render", "(Re)create the index page.") {
      opts.action = :render
    }

    opo.on("--all", "Like --import, --fetch, and --render .") {
      opts.action = :all
    }

    opo.on("--delete-ignored", "Delete all ignored mentions.") {
      opts.action = :delete_ignored
    }

    opo.on("--delete-unsolicited [TRESHOLD]", Integer,
           "Delete unsolicited tweets unless < THRESHOLD"
          ) { |threshold|
      opts.action = :delete_unsolicited
      opts.reply_cutoff = threshold if threshold
      assert("THRESHOLD must be >= 0") { opts.reply_cutoff >= 0 }
    }

    opo.on("--truncate-threads [DAYS]", Integer,
           "Truncate all threads after DAYS days."
          ) { |days|
      opts.action = :truncate_thread
      opts.thread_age = days if days
      assert("THRESHOLD must be >= 0") { opts.thread_age >= 0 }
    }

    opo.on("--page-size COUNT", Integer,
           "Set number off tweets per html file; <= 0 means one big page."
          ) { |count|
      opts.page_size = count
    }

    opo.on("-n","--dry-run","No deletion! (Warning: doesn't affect --render)"){
      opts.dry_run = true
    }

    opo.on("-f","--do-it", "Do the dangerous thing. --dry-run overrides.") {
      opts.do_it = true
    }
  end.parse!

  opts.do_it = false if opts.dry_run
  
  return opts
rescue OptionParser::ParseError => e
  die "Option error: #{e}"
end



def main
  opts = getopts()

  username = ARGV.shift
  username or die "Usage: #{$0} [options] <username> [...tweet files...]"

  say "Loading current tweets..."
  tweets = TweetSet.new(username)
  say "Done."

  case opts.action
  when :import
    import(tweets, ARGV.dup)
  when :fetch
    fetch(tweets)
  when :render
    render(tweets, opts.page_size)
  when :all
    import(tweets, ARGV.dup)
    fetch(tweets)
    render(tweets, opts.page_size)
  when :delete_ignored
    delete_ignored_threads(tweets, opts.do_it)
  when :delete_unsolicited
    delete_all_unsolicited(tweets, opts.reply_cutoff, opts.do_it)
  when :truncate_thread
    truncate_thread(tweets, opts.thread_age, opts.do_it)
  else
    die "No action specified."
  end

  say "Done."
  say tweets.stats_desc
  say "memsize_of_all:     #{ObjectSpace::memsize_of_all}"
end


main
