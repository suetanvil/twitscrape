
# This came from an old Mastodon archive that had been converted to a
# web page with a script.  I have no idea who wrote the script so I
# can't attribute them, but whoever you are, thanks!.
RENDER_STYLE = <<EOF
.status {
    width: 54ch;
    position: relative;
    min-height: 128px;
    margin:auto auto 4em auto;
    border: 1px solid #999;
    border-radius: 16px;
    padding:8px;
    background: rgba(0,0,0,0.75);
}

.status::before{
    content: url('avatar.png');
    position: absolute;
    right: 100%;
}

.status__summary {
    width: 100%;
    background: #333
}

.status__summary::after {
    content: '[Click to Open]';
    display: block;
}

.status__date {
    text-align: right;
}
.status__meta {
    text-align: left;
}
.status__content {
}
.status__media {
    width:100%;
 }

.status__image {
    max-width: 100%;
    width:100%;
    min-width:100%;
}

#header {
    background: rgba(0,0,0,0.75);
    text-align: center;
    padding-bottom: 16px;
}

body {
    margin:0;
    background:#333;
    background-image: url('header.png');
    background-size: cover;
    background-attachment: fixed;
    color: #fff1e8;
    line-height: 1.4;
}

* {
    box-sizing: border-box;
}

.mention {
    color: rgb(150,255,140);
}

.mention_from {
    color: #FFBF00;
//    background: rgba(50,50,50,0.75);
}

a {
    color: rgb(150,255,140)
}
EOF

