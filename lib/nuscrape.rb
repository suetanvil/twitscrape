#!/usr/bin/env ruby

require 'util'
require 'tweets'
require 'launder_ops'

require 'optparse'



def getopts
  opts = new_struct(
    max: nil,
    no_rescrape: false,
    skip_gallery: false,
    skip_fetch: false,
    skip_render: false,
    page_size: 1000,

    scrape_mentions: false,
    mention_end: Time.now,
    mention_chunk: 10*24*60*60,        # ten days

    twint_tries: 3,
  )

  OptionParser.new do |opo|
    opo.banner = "Usage: #{$0} [options]"

    opo.on("--max COUNT", Integer, "Maximum number of tweets to scrape.") { |c|
      opts.max = c
    }

    opo.on("--skip-fetch", "Skip fetching thumbnails.") {
      opts.skip_fetch = true;
    }

    opo.on("--no-rescrape", "Don't scrape if results are already present.") {
      opts.no_rescrape = true
    }

    opo.on("--skip-gallery", "Skip running gallery-dl.") {
      opts.skip_gallery = true
    }

    opo.on("--skip-render", "Skip generating html.") {
      opts.skip_render = true
    }

    opo.on("--page-size COUNT", Integer,
           "Set number off tweets per html file; <= 0 means one big page."
          ) { |count|
      opts.page_size = count
    }

    # TODO: also add --mention-start

    opo.on("--mentions", "Scrape mentions as well.") {
      opts.scrape_mentions = true
    }

    opo.on("--mention-end DATE", "Stop scraping mentions after DATE.") {
      |m_end|

      opts.mention_end = DateTime.parse(m_end).to_time
      opts.scrape_mentions = true
    }

    opo.on("--twint-tries TRIES", Integer,
           "Number of times to retry fetching a mention range. Default = 3") {
      |tries|
      opts.twint_tries = tries
      assert("Invalid tries value!") { opts.twint_tries > 0 }
    }

    opo.on("--range-size DAYS", Integer,
           "Scrape mentions in chunks of size DAYS") {
      |range|
      assert{range > 0}
      opts.mention_chunk = range*60*60*24
      opts.scrape_mentions = true
    }

  end.parse!

  return opts
rescue OptionParser::ParseError => e
  die "Option error: #{e}"
end

def run_twint(filename, opts, args, force: false)
  if !force && opts.no_rescrape && File.exist?(filename)
    puts "Found '#{filename}'; skipping."
    return
  end

  tmp_filename = "#{filename}.#{SecureRandom.hex(6)}.tmp.json"

  cmd = %W{twint --json --count --stats} + args
  cmd += %W{--limit #{opts.max}} if opts.max
  cmd += %W{-o #{tmp_filename}}

  for try in 0..opts.twint_tries
    begin
      FileUtils.rm(tmp_filename) if File.exist? tmp_filename
      run(cmd)
      catfile(tmp_filename, filename) if File.exist? tmp_filename
      break
    rescue ExternalToolFailed => e
      say "Attempt #{try+1}/#{opts.twint_tries} failed!"
      log("twint failed:")
      log(*e.to_s.split(/\n/))

      # Rethrow if we've run out of attempts
      raise e if try+1 >= opts.twint_tries
    end
  end

  FileUtils.rm(tmp_filename) if File.exist? tmp_filename
end


def scrape_timeline(opts, username, filename, include_retweets)
  args = %W{-u #{username} --full-text}
  args += %W{--retweet} if include_retweets

  run_twint(filename, opts, args)
end

def compute_ranges(startdate, enddate, chunk_size)
  s = startdate
  ranges = []
  while s < enddate
    e = s + chunk_size
    ranges.push [s, e]
    s = e
  end

  return ranges
end

def scrape_mention_range(opts, username, startdate, enddate, filename)
  s, e = [startdate, enddate].map{|d| d.strftime("%F %T")}
  args = %W{-s #{''} --to #{username} --full-text --since #{s} --until #{e}}

  run_twint(filename, opts, args, force: true)
end

def scrape_mentions(opts, username, startdate, filename)

  # We check here since we need to override run_twint's behaviour.
  if opts.no_rescrape && File.exist?(filename)
    puts "Found '#{filename}'; skipping."
    return
  end

  ranges = compute_ranges(startdate, opts.mention_end, opts.mention_chunk)

  for startdate, enddate in ranges
    say "Scraping #{startdate} -> #{enddate}"
    scrape_mention_range(opts, username, startdate, enddate, filename)
  end
end


def gallery_dl(tweets)
  # TODO: this should really be computed by TweetSet; currently, Tweet
  # does it, which is a problem if we don't know for sure that there's
  # a tweet present.
  gdl_assets = File.join(tweets.username, ASSETS_GDL)

  cmd = %W{gallery-dl http://twitter.com/#{tweets.username} -D #{gdl_assets}}
  run(cmd, tolerate_failure: true)
end


def fname(username, suffix) = "#{username}-#{suffix}.json"


def main
  opts = getopts()
  username = ARGV.shift or die "No username given."

  log("Scraping #{username}", "", "")

  say "Scraping profile..."
  scrape_timeline(opts, username, fname(username, 'tweets'), false)

  say "Scraping retweets..."
  scrape_timeline(opts, username, fname(username, 'retweets'), true)


  # We need to import the initial set of tweets now in order to get
  # the date of the first tweet
  tweets = TweetSet.new(username)
  tweets.import_verbose!(fname(username, 'tweets'))
  tweets.import_verbose!(fname(username, 'retweets'))
  tweets.save!

  if opts.scrape_mentions
    say "Scraping mentions..."

    first = tweets.tweets_sorted()[0]
    if first
      startdate = first.timestamp
    else
      startdate = DateTime.parse("2006-01-01")    # Year of Twitter's launch
      say "No tweets found.  Starting from #{startdate}."
    end

    scrape_mentions(opts, username, startdate, fname(username, 'mentions'))
    tweets.import_verbose!(fname(username, 'mentions'))
    tweets.save!
  end

  # Extended media fetch
  unless opts.skip_gallery
    say "Updating gallery."
    gallery_dl(tweets)
  end

  # Fetch dependencies
  unless opts.skip_fetch
    say "Fetching thumbnails."
    fetch(tweets)
    tweets.save!
  end

  # Render
  unless opts.skip_render
    say "(Re)rendering html pages."
    render(tweets, opts.page_size)
  end

  log("Finished scraping #{username}", "", "")
end


main()
