
require 'json'
require 'fileutils'
require 'set'
require 'open-uri'
require 'securerandom'
require 'pathname'

require 'tweet_common.rb'
require 'tweet.rb'

class TweetSet
  attr_reader :username, :last_read_count, :user_id
  def initialize(username)
    @username = username
    @assets = File.join(username, ASSETS)
    @gdl_assets = File.join(username, ASSETS_GDL)
    @trash = File.join(username, TRASH)
    @tweets = {}
    @deleted_tweets = {}
    @last_read_count = 0
    @user_id = nil

    make_paths()
    read_tweets()
    read_deleted()

    set_user_id()
  end

  private

  def make_paths
    FileUtils.mkdir_p(@assets)
    FileUtils.mkdir_p(@gdl_assets)
    FileUtils.mkdir_p(@trash)
  end

  def read_tweets
    @last_read_count = 0
    throb("Reading tweets...") { |throbfn|
      Dir.each_child(@assets) do |item|
        throbfn.call

        next unless File.directory? File.join(@assets, item)
        next unless item =~ /^\d+$/

        id = item.to_i
        tweet = Tweet.new(@username, id: id)

        @tweets[tweet.id] = tweet
        @last_read_count += 1
      end
    }
  end

  def read_deleted()
    deltwts = File.join(@username, DELETED_TWEETS_FILE)
    return unless File.exist?(deltwts)

    read_deleted_oldstyle(deltwts) and return

    throb("Loading deleted tweets...") do |throbfn|
      open(deltwts, "r") { |fh|
        for tweet_txt in fh
          tweet = TweetStruct.from_hash(JSON.parse(tweet_txt))
          @deleted_tweets[tweet.id] = tweet
          throbfn.call
        end
      }
    end
  end

  # Old-style == a single hash
  def read_deleted_oldstyle(deltwts)
    line = open(deltwts, "r") { |fh| fh.readline }
    return false unless line =~ /^\s*\{\s*$/

    say "#{deltwts} is in the old format..."

    @deleted_tweets = {}
    dts = open(deltwts, "r") { |fh| JSON.load(fh)}
    for twt_hash in dts.values
      twt = TweetStruct.from_hash(twt_hash)
      @deleted_tweets[twt.id] = twt
    end

    return true
  end


  # Find the numeric user ID; we use this because the handle change.
  # (That being said, I don't know if twint can follow those changes).
  def set_user_id()
    return if @user_id
    return if @tweets.empty?

    for tweet in @tweets.values
      if tweet.username == @username
        @user_id = tweet.user_id
        return
      end
    end
  end


  public

  def inspect = "TweetSet(#{@username}, #{@tweets.size})"

  def size = @tweets.size

  def stats_desc = "Count: #{size}; last_read_count: #{@last_read_count}"

  def [](id) = @tweets[id]
  def has_key?(id) = @tweets.has_key?(id.to_i)

  def by_me?(tweet) = tweet.username == @username || tweet.user_id == @user_id

  def import!(twint_json, silent = true)
    dupes = 0
    localdupes = 0
    imports = Set.new

    File.open(twint_json, "r") do |fh|
      throb("Importing #{twint_json}...", silent: silent) { |throbfn|
        fh.each_line do |line|
          tweet = Tweet.new(@username, tweethash: JSON.parse(line))
          if @tweets.has_key?(tweet.id)
            dupes += 1
            localdupes += 1 if imports.include?(tweet.id)
          else
            @tweets[tweet.id] = tweet
          end

          imports.add(tweet.id)

          throbfn.call
        end
     }
    end

    return [dupes, localdupes]
  end

  def import_verbose!(twint_json)
    say "Reading #{twint_json}..."
    dupes, localdupes = import!(twint_json, false)
    say "#{twint_json}; dupes = #{dupes}; localdupes = #{localdupes}"

    return [dupes, localdupes]
  end


  def save!
    count = 0
    throb("Saving...") { |throbfn|
      for tweet in @tweets.values
        throbfn.call
        tweet.save! and count += 1
      end
    }

    save_as_one_file(File.join(@username, ALL_TWEETS_FILE));
    save_deleted!

    return count
  end

  private

  def save_deleted!
    return if @deleted_tweets.empty?

    throb("Saving deleted tweets...") do |throbfn|
      File.open(File.join(@username, DELETED_TWEETS_FILE), "w") {|fh|
        @deleted_tweets.each_value{|twt|
          fh.puts(JSON.generate(twt.to_h))
          throbfn.call()
        }
      }
    end
  end

  public


  def delete_all!(ids)
    media = []
    for id in ids
      tweet = self[id]
      assert("No tweet #{id}"){tweet}

      media += tweet.gallery_media

      move_to_trash(tweet.dir_path)

      @deleted_tweets[id] = tweet.tweethash
      @tweets.delete(id)
    end

    for media_file in media
      move_to_trash(media_file)
    end
  end

  def delete!(id)
    delete_all!([id])
  end


  private

  def move_to_trash(file)
    destbase = File.basename(file)

    count = 1
    while File.exist?( File.join(@trash, destbase) )
      destbase = File.basename(file) + ".#{count}"
      count += 1
    end

    destname = File.join(@trash, destbase)

    #say "Moving '#{file}' to trash"
    FileUtils.mv(file, destname)
  end


  public



  #
  # Queries
  #

  def tweets_sorted = @tweets
    .keys
    .sort
    .map{|k| @tweets[k]}


  # Return contents as an array of threads where each thread is an
  # array of articles.  Threads are sorted newest-to-oldest by
  # earliest tweet and tweets in the thread itself are sorted
  # oldest-to-newest.  Standalone tweets are their own thread.
  def conversations
    threads = {}
    for tweet in tweets_sorted
      id = tweet.conversation_id.to_i
      threads[id] = [] unless threads.has_key?(id)
      threads[id].push(tweet)
    end

    return threads.values.reverse
  end

  # Return two lists of threads: the first where this user is a
  # participant; the other where this user is *not*.
  def active_and_ignored_threads
    active = []
    ignored = []

    for thread in conversations
      hasme = thread.detect{|tw| by_me? tw}
      if hasme
        active.push thread
      else
        ignored.push thread
      end
    end

    return [active, ignored]
  end

  # For each thread that @username participates in, return two arrays:
  # the first contains @username's tweets and those of people
  # @username replies to in-thread.  The second is everyone else.
  def active_threads_split_by_user_attention

    active, _ = active_and_ignored_threads()

    result = []
    for thread in active
      next unless thread.size > 0

      # First, extract @username's tweets
      my_tweets = thread.select{|tw| by_me?(tw)}
      next if my_tweets.empty?  # shouldn't happen, but anyway, it's junk

      participants = Set.new([@username])    # username and user_id of each participant
      participants.add(@user_id) if @user_id

      # Now, add everyone they reference
      for twt in my_tweets
        participants += twt.all_mentioned(user_id: false)    # username
        participants += twt.all_mentioned(user_id: true)     # user id num
        participants.add(twt.replyee)
      end

      active = []
      ignored = []
      for tweet in thread
        if (participants & [tweet.username, tweet.user_id].to_set).empty?
          ignored.push tweet
        else
          active.push tweet
        end
      end

      result.push([active, ignored])
    end

    return result
  end


  # Save all live tweets as a JSON list
  def save_as_one_file(filename)
    say "(Re)creating #{filename}"
    open(filename, "w") { |fh|
      tweet_contents = tweets_sorted()
        .map{|tweet| tweet.tweethash}
      fh.write( JSON.pretty_generate( tweet_contents ) )
    }
  end

  # Media management

  def cull_duplicates!
    dupes = []

    STDERR.write("\n")
    count = 0
    for tweet in @tweets.values
      dupes += tweet.cull_duplicates!
      STDERR.write("\r#{count}/#{@tweets.size}")
      STDERR.flush
      count += 1
    end

    STDERR.write("\n")

    return dupes
  end

  def fetch_photos
    candidates = @tweets.values.select{|twt| twt.has_unfetched? }

    failed = []
    for tweet in candidates
      failed += tweet.fetch_photos
    end

    return failed
  end


  #
  # Stats
  #

  def get_stats
    stats = new_struct(
      total: 0,
      total_by_account: 0,
      total_replies: 0,

      threads: 0,
      threads_started_by_me: 0,
      max_thread_length: 0,
      mean_thread_length: 0,

      ignored_threads: 0,
      ignored_thread_tweets: 0,
      active_threads: 0,

      active_threads_with_unsolicited: 0,
      ratio_unsolicited: 0,
      total_solicited: 0,
      total_actv_unsolicited: 0,

    )

    for tweet in @tweets.values
      stats.total += 1
      stats.total_by_account += 1 if tweet.user_id == @user_id
      stats.total_replies += 1 if tweet.reply?
    end

    total_thread_lengths = 0
    for thread in conversations
      next if thread.size < 2   # Trivial threads are ignored.
      stats.threads += 1
      total_thread_lengths += thread.size
      stats.threads_started_by_me += 1 unless thread[0].reply?
      stats.max_thread_length = [stats.max_thread_length, thread.size].max
    end

    stats.mean_thread_length = (total_thread_lengths.to_f / stats.threads).round 1

    ats, its = active_and_ignored_threads()
    stats.ignored_threads = its.size
    stats.active_threads = ats.size
    stats.ignored_thread_tweets = its.flatten.size

    # Consistency check 1
    ait_total = ats.flatten.size + its.flatten.size
    assert("AITS returned #{ait_total}; expecting #{stats.total}") {
      ait_total == stats.total
    }

    for active, ignored in active_threads_split_by_user_attention()
      stats.active_threads_with_unsolicited += 1 unless ignored.empty?
      stats.total_actv_unsolicited += ignored.size
      stats.total_solicited += active.size
    end

    assert("Inconsistency between solicited/unsolicted counts and total") {
      total = stats.total_solicited +
        stats.total_actv_unsolicited +
        stats.ignored_thread_tweets

      total == stats.total
    }

    stats.ratio_unsolicited = (stats.total_actv_unsolicited.to_f / stats.total).round(3)

    return stats
  end
end
