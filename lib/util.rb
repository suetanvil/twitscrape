
require 'open3'


# Diagnostic/verbose messages; for now, just a wrapper around puts.
def say(msg) = puts(msg)

def die(msg)
  puts msg
  exit(1)
end

def assert(msg = "", &block)
  return if block.call()
  die "Assertion failure: #{msg} #{block.source_location.join(':')}"
end

# FileUtils.cmp but tolerates missing files
def files_eq(file1, file2)
  return false unless File.exist?(file1) && File.exist?(file2)
  FileUtils.cmp(file1, file2)
end

# Return a new Struct instance whose fields are the given keys, set to
# the corresponding values.
def new_struct(**args)
  keys, values = args.to_a.transpose
  return Struct.new(*keys).new(*values)
end


LOGFILE = "scrape.log"

def log(*text)
  now = Time.now.strftime("%FT%T")
  open(LOGFILE, "a") { |fh|
    for line in text
      line = line.chomp
      fh.puts "[#{now}] #{line}"
    end
  }
end


def catfile(src, dest)
  open(src, "r") do |src_fh|
    open(dest, "a") do |dest_fh|
      dest_fh.write(src_fh.readline) until src_fh.eof?
    end
  end
end



class ExternalToolFailed < RuntimeError; end

def run(cmd, throb: true, tolerate_failure: false)
  cmdstr = cmd.join(' ')
  say "RUN: #{cmdstr}"

  throbs = %w{/ - \\ |}
  count = 0

  last = Time.now
  status = nil
  lines = ["RUN: #{cmdstr}"]
  Open3.popen2e(*cmd) { |stdin, stdouterr, wait_thr |
    for line in stdouterr
      lines.push("> #{line.chomp}")

      next unless throb && (Time.now - last) > 0.5
      STDERR.write(" [#{throbs[count % throbs.size]}] #{lines.size}     \r")
      STDERR.flush
      count += 1
      last = Time.now
    end

    status = wait_thr.value
  }
  STDERR.write("\n")

  lines.push "Exit status: #{status.exitstatus}"
  lines.push ""

  log(*lines)

  say "ERROR STATUS: #{status.exitstatus}" if tolerate_failure && !status.success?
  say "Done."

  tolerate_failure || status.success? or
    raise ExternalToolFailed.new(
            "CMD: #{cmd.join(' ')}\nOutput:\n#{lines.join("\n")}"
          )

  return status
end


def throb(
      prefix,                       # Textual heading
      update_time:      0.5,        # Delay between throbs
      silent:           false,      # Print nothing
      delay:            5.0,        # Wait this long before starthing throb
      &block)
  last = Time.now + delay
  count = 0

  throbber_idx = 0
  throbber = ['/', '-', '\\', '|']

  throbfn = proc{
    next if silent

    count += 1

    now = Time.now
    if now > last + update_time
      last = now

      throb = "[#{throbber[throbber_idx % throbber.size]}]"
      throbber_idx += 1

      STDERR.write("\r #{prefix} #{throb} - #{count}       \r")
      STDERR.flush
    end
  }

  result = block.call(throbfn)

  STDERR.puts unless silent

  return result
end
