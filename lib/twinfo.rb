#!/usr/bin/env ruby

require 'util'
require 'renderer'
require 'tweets'
require 'launder_ops'

require 'optparse'

=begin
def getopts
  opts = new_struct(
    action: nil,
    page_size: 1000,
  )

  OptionParser.new do |opo|
    opo.banner = "Usage: #{$0} [options]"

    opo.on("--import", "(Re)import files.") {
      opts.action = :import
    }

    opo.on("--fetch", "Fetch attached images.") {
      opts.action = :fetch
    }

    opo.on("--render", "(Re)create the index page.") {
      opts.action = :render
    }

    opo.on("--all", "Like --import, --fetch, and --render .") {
      opts.action = :all
    }

    opo.on("--page-size COUNT", Integer,
           "Set number off tweets per html file; <= 0 means one big page."
          ) { |count|
      opts.page_size = count
    }

  end.parse!

  return opts
end
=end


def main
  #opts = getopts()

  username = ARGV.shift
  username or die "Usage: #{$0} [options] <username> [...tweet files...]"

  say "Loading current tweets..."
  tweets = TweetSet.new(username)

  puts "Stats:"
  for stat, value in tweets.get_stats.to_h
    puts "    #{stat} = #{value}"
  end
  
  say "Done."
end


main
