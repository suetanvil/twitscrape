
# Dependencies used by Tweet and TweetSet

TWEETFILE = "tweet.json"
DUPES_FILE = 'imagedupes.json'

ALL_TWEETS_FILE = 'all-tweets.json'
DELETED_TWEETS_FILE = 'deleted-tweets.json'

ASSETS = 'assets'
ASSETS_GDL = 'assets_gallery_dl'
TRASH = 'trash'


# Cache class for the entries of an ASSETS_GDL directory
class GalleryDirCache
  def initialize(path)
    @path = path
    @entries_mtime = nil
    @entries = {}

    assert{File.directory?(path)}

    say "Making cache for #{path}"

    refresh!
  end

  def refresh!
    @entries_mtime = path_mtime()
    @entries = {}

    for entry in Dir.entries(@path)
      next unless entry =~ /^(\d+)_\d\./
      id = $1.to_i

      @entries[id] = [] unless @entries.has_key?(id)
      @entries[id].push entry
    end
  end

  def path_mtime = File.stat(@path).mtime

  def [](id)
    refresh! if path_mtime > @entries_mtime
    return @entries.fetch(id, [])
  end

  # Instance management

  @caches = {}
  def self.for(path)
    path = File.absolute_path(path)
    @caches[path] = self.new(path) unless @caches.has_key?(path)
    return @caches[path]
  end

end
