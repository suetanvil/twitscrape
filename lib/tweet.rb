
# Class representing an individual Tweet

require 'json'
require 'fileutils'
require 'set'
require 'open-uri'
require 'securerandom'
require 'pathname'

require 'tweet_common.rb'

TweetFields = [
  :id, :conversation_id, :created_at, :date, :time,
  :timezone, :user_id, :username, :name, :place, :tweet, :language,
  :mentions, :urls, :photos, :replies_count, :retweets_count,
  :likes_count, :hashtags, :cashtags, :link, :retweet, :quote_url,
  :video, :thumbnail, :near, :geo, :source, :user_rt_id, :user_rt,
  :retweet_id, :reply_to, :retweet_date, :translate, :trans_src,
  :trans_dest
]

TweetStruct = Struct.new( *TweetFields ) do
  def self.from_hash(tweethash)
    result = self.new
    for k, v in tweethash
      v = Empty if v == []
      result[k] = v
    end

    return result
  end
end

Empty = [].freeze


class Tweet
  # Must provide id or tweet body. If an ID is given, that is used to
  # retrieve the Tweet from the archive.
  def initialize(root, id: nil, tweethash: nil)
    @root = root
    @id = nil
    @tweetstruct = nil
    @image_dupes = {}
    @image_dupes_changed = false

    assert("Must provide exactly ONE of an ID or tweet body!") {
      !!id ^ !!tweethash
    }

    if tweethash
      @tweetstruct = TweetStruct.from_hash(tweethash).freeze
      @id = tweethash['id']
    else
      @id = id
      load_tweet!()
    end
  end

  private

  # Produces a tweet struct from the corresponding JSON hash.

  def load_tweet!
    # assumes @id is initialized
    th = open(file_path(), "r") { |fh| JSON.load(fh) }
    @tweetstruct = TweetStruct.from_hash(th).freeze

    dfp = dupes_file_path()
    if File.exist? dfp
      @image_dupes = open(dfp, "r") { |fh| JSON.load(fh) }
    end

    assert{ @tweetstruct['id'] == @id }
  end

  public

  # Queries:

  def tweethash = @tweetstruct  # not a hash, but close enough

  def asset_path        = File.join(@root, ASSETS)
  def asset_gdl_path    = File.join(@root, ASSETS_GDL)
  def dir_path          = File.join(@root, ASSETS, @id.to_s)
  def file_path         = File.join(dir_path, TWEETFILE)
  def dupes_file_path   = File.join(@root, ASSETS, @id.to_s, DUPES_FILE)

  def timestamp = Time.parse(@tweetstruct['created_at'])

  def retweet? = @tweetstruct['tweet'][0..3] == "RT @"
  def reply? = !!replyee

  def replyee
    m = /^\@(\w+)/.match(self.tweet) or return nil
    return m[1]
  end

  # Return all mentioned accounts in "mentions" and "reply_to"
  # fields. If user_id: is true, returns ID number; otherwise, it's
  # the username.
  def all_mentioned(user_id: false)
    key = user_id ? "id" : "screen_name"
    result = []
    for user in reply_to() + mentions()
      assert{user[key]}
      result.push( user[key] )
    end

    return result
  end


  def inspect = "Tweet(#{self.id}, '@#{self.username}', '#{self.tweet[0..20]}...')"


  # Saving:

  # Save this tweet's contents if not already defined.  Return true if
  # something was saved, false otherwise
  def save!
    FileUtils.mkdir_p( dir_path() )

    saved = false
    if @image_dupes_changed
      open(dupes_file_path(), "w") {
        |fh| fh.write(JSON.pretty_generate(@image_dupes))
      }
      saved = true
      @image_dupes_changed = false
    end

    # We only write the tweet if it doesn't already exist.  Tweet
    # contents are notionally immutable.
    path = file_path()
    unless File.exist?(path)
      open(path, "w") { |fh| fh.write(JSON.pretty_generate(@tweetstruct.to_h)) }
      saved = true
    end

    return saved
  end




  # Media file paths

  # Local filenames to use for attached media; files don't necessarily
  # exist.
  def photo_paths = self.photos.map{|purl| url_to_path(purl) }

  # Return the list of media files retrieved from URLs in the tweets
  def fetched_media = photo_paths.select{|path| File.exist? path}

  def known_dupes = @tweetstruct.keys

  # Return any corresponding media files in the gallery-dl directory
  def gallery_media
    paths = GalleryDirCache.for( asset_gdl_path() )[self.id]
    return paths.map{|fn| File.join(@root, ASSETS_GDL, fn)}
  end

  def media_files
    fetched = fetched_media()
    gallery = gallery_media()

    # Prioritize images in both sets.  These are the ones that got
    # deleted after --fetch
    common = @image_dupes.values

    # ...and delete them from gallery
    gallery -= common

    return [common, fetched, gallery]
  end

  # Return two lists: fetched (i.e. the media in the tweet itself) and
  # gallery--dl items that are *not* duplicates of the fetched.  The
  # fetched are returned in the original order because Debbie really
  # wants that.
  def fetched_and_others
    fetched = []
    for path in self.photos.map{|url| url_to_path(url)}
      if File.exist?(path)
        fetched.push path
      elsif @image_dupes.has_key?(path)
        fetched.push(@image_dupes[path])
      else
        puts "Missing media file: #{path}"
      end
    end

    gallery = gallery_media().reject{|path| fetched.include? path}

    return [fetched, gallery]
  end



  private

  def url_to_path(url) = File.join(@root, ASSETS, id.to_s, File.basename(url))

  public


  # Media file management

  def cull_duplicates!
    removed = []

    galmed = gallery_media()
    for fetched in fetched_media()
      for gi in galmed
        if files_eq(fetched, gi)
          @image_dupes[fetched] = gi
          @image_dupes_changed = true

          FileUtils.rm(fetched)
          removed.push(fetched)
          break
        end
      end
    end

    return removed
  end

  def fetch_photos
    failed = []

    for url in missing_photos()
      fetch(url) or failed.push( url_to_path(url) )
    end

    return failed
  end

  def has_unfetched? = !missing_photos.empty?

  private

  def missing_photos()
    missing = []

    for url in photos()
      path = url_to_path(url)
      missing.push(url) unless
        File.exists?(path) || @image_dupes.has_key?(path)
    end

    return missing
  end

  def fetch(url)
    filename = url_to_path(url)

    if File.exist?(filename)
      say "Duplicate image: #{filename}"
      return
    end

    say "Fetching '#{url}'"

    filename_tmp = filename + ".#{rand(0xFFFFFF)}.tmp"
    contents = URI.open(url, "rb") {|fh| fh.read }
    File.open(filename_tmp, "wb") { |ofh| ofh.write(contents) }
    FileUtils.mv(filename_tmp, filename)

    return true
  rescue StandardError => e
    puts "Error: #{e}"
    puts "Skipping #{url}"
    return false
  end


  public

  # Tweethash access

  %i{
     id created_at date time timezone user_id
     username name place tweet language mentions urls photos replies_count
     retweets_count likes_count hashtags cashtags link retweet quote_url
     video thumbnail near geo source user_rt_id user_rt retweet_id reply_to
     retweet_date translate trans_src trans_dest
    }
    .each { |msg|
      msgtxt = msg
      define_method(msg) {return @tweetstruct["#{msgtxt}"]} unless
        method_defined?(msg)
    }

  # Special case; twint gives us tweet IDs as ints but conversation_id
  # as a string consisting of the digits of the int.  We normalize
  # that here.
  def conversation_id = @tweetstruct['conversation_id'].to_i

end
